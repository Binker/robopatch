# Setup Robopatch on Linux

> **Push out a script to robopatch and boot a RHEL-box if it's required.**  
> **Based on a file touched by an external scheduling tool, a service is listening on that file and triggers the OS patch script.**  


## Run like this:
`$ ansible-playbook -i <fqdn1>,<fqdn2>,<etc...>, enable_robopatch.yml`  

---

#### Options  
Main options are set in the `./group_vars/all.yml` file.  
Role specfic options are set in (you guessed it..) the '.roles/\<role\>/vars/main.yml' file.  

---
#### Scripts:

1. The "robopatch" script

 IF the trigger file is modified, the ["robopatch"](https://gitlab.com/Binker/robopatch/-/blob/master/roles/patch/templates/robopatch.sh.j2) script does a *yum update*.  
 It also contains the "exit_check" on the yum command, and will write the error code to the log if it fails.  
 The script utilizes 'yum-utils', and specifically the 'needs-restarting' command.  
 If there is changes to things like dbus, glibc, systemd, linux-firmware or the kernel, the server reboots.  
 Information about what the script does, based on that check, is written to the log file.


#### Service:
Two systemd unit files are deployed.  
1. robopatchd.path
2. robopatchd.service

The first service listens for changes the scheduler makes to the trigger file.  
If that file is modified in any way (even a 'touch' or 'rm'), it will notify the second unit file, which will trigger the "robopatch" OS update script.  

#### The log file:

On target hosts the log file can be found here: "/var/log/robopatch_logfile.log".  
Splunk normally picks up everything under "/var/log/*" on Linux hosts. This way you can set up an alert in Splunk, and be notified if anything fails.  

#### Crontab:  

- *c_name*:
    - Inserts a comment into the crontab with the name and description of the job.
- *c_state*:
    - Sets the job to a "present" or "absent" state on the target host.
- *c_weekday*:
    - Day of the week that the job should run (0-6 for Sunday-Saturday, *, etc)
- *c_minute*:
    - Minute when the job should run (0-59, *, */2, etc)
- *c_hour*:
    - Hour when the job should run (0-23, *, */2, etc)
- *c_day*:
    - Day of the month the job should run (1-31, *, */2, etc)  

More information can be found in the official [Ansible documentation](https://docs.ansible.com/ansible/latest/modules/cron_module.html).  
